// Name: Moof's Switch Saturation and Value v1.0
// Submenu: Color
// Author: MoofEMP
// Title: Switch Saturation and Value
// Version: 1.0
// Desc: Switches the saturation and value channels of the selection
// URL: https://gitlab.com/moofemp-pdn-plugins/switch-saturation-and-value

void Render(Surface dst,Surface src,Rectangle rect){
    ColorBgra CurrentPixel;
    for(int y=rect.Top;y<rect.Bottom;y++){
        if(IsCancelRequested) return;
        for(int x=rect.Left;x<rect.Right;x++){
            CurrentPixel=src[x,y];

            HsvColor pxhsv=HsvColor.FromColor(CurrentPixel.ToColor());  //converting RGB to HSV
            pxhsv=new HsvColor(pxhsv.Hue,pxhsv.Value,pxhsv.Saturation); //note the HVS order
            ColorBgra NewPixel=ColorBgra.FromColor(pxhsv.ToColor());    //HSV (HVS) back to RGB
            NewPixel.A=CurrentPixel.A;                                  //preserve alpha

            dst[x,y]=NewPixel;
        }
    }
}
