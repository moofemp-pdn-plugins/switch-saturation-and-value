# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Fix
- codelab interpreting source code as plaintext instead of c#

## [1.0] - 2021-10-03
### Changed
- Generally cleaned up repository
- Inclusion of samples in repository
- DLL now tracks with LFS
- Filenames use spaces
- Migrated to moofemp-pdn-plugins/switch-saturation-and-value
- Bagel test image replaced with mandrill

## [0.2] - 2020-03-30
### Changed
- switched to two-tier version numbering to be consistent with dll version
- updated url metadata to link directly to this repo
- renamed Switch_Saturation_and_Value_icon.png to icon.png
### Removed
- superfluous `Rectangle selection` declaration

## [0.1] - 2020-03-30
### Added
- source code for file; iterates for each pixel and swaps saturation and value channels
- compiled dll
- effect icon
